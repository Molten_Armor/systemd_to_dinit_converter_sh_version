#!/usr/bin/bash
set -ueo pipefail

# systemd unit "key" ref map
SYSTEMD_REF_MAP=(
'Documentation'
'Type'
'Description'
'Wants'
'Requires'
'Requisite'
'BindsTo'
'PartOf'
'Upholds'
'Before'
'After'
'OnSuccess'
'StartLimitBurst'
'StartLimitIntervalSec'
'Alias'
'WantedBy'
'RequiredBy'
'PIDFile'
'ExecStart'
'ExecStop'
'TimeoutStartSec'
'TimeoutStopSec'
'Restart'
'EnvironmentFile'
'User'
'Group'
'WorkingDirectory'
'LimitCORE'
'LimitDATA'
'LimitNOFILE'
'UtmpIdentifier'
'KillSignal'
)

warn() {
    printf '\e[31m%s\e[0m\n' "$1" 1>&2
}

# Systemd unit file
if [ -f "$1" ]; then
    UNITFILE="$1"
else
    warn "Usage: $0 <Unit file>"
    exit 1
fi
# Values in fact
declare -a KVS=()

parse_time() {
    if (expr "$1" : '[[:digit:]]*$' > /dev/null); then
        printf '%s' "$1"
        return 0
    fi
    declare sec=0
    declare mem=''
    declare what=''
    declare -i letter=0
    declare -a times=()
    for ch in $(fold -w 1 <<< "$1"); do
        if [ -z "${ch}" ]; then
            continue
        elif (expr "${ch}" : '[[:digit:]]*$' > /dev/null); then
            if [ "${letter}" -eq 0 ]; then
                # Here is next entry
                mem="${mem}${ch}"
                continue
            else
                times+=("${what//[[:blank:]]}-${mem//[[:blank:]]}")
                mem="${mem}${ch}"
                what=''
                # Reset letter for new entry
                letter=0
                continue
            fi
        elif (expr "${ch}" : '[[:alpha:]]*$' > /dev/null); then
            what="${what}${ch}"
            letter=1
        fi
        times+=("${what//[[:blank:]]}-${mem//[[:blank:]]}")
    done

    for item in "${times[@]}"; do
        case "${item%%'-'*}" in
            'μs' | 'us' | 'usec')
                sec="$(bc -s <<< "scale=6; ${sec} + ${mem} / 1000000")"
                ;;
            'ms' | 'msec')
                sec="$(bc -s <<< "scale=6; ${sec} + ${mem} / 1000")"
                ;;
            's' | 'sec' | 'second' | 'seconds')
                sec="$(bc -s <<< "scale=6; ${sec} + ${mem}")"
                ;;
            'm' | 'min' | 'minute' | 'minutes')
                sec="$(bc -s <<< "scale=6; ${sec} + ${mem} * 60")"
                ;;
            'h' | 'hr' | 'hour' | 'hours')
                sec="$(bc -s <<< "scale=6; ${sec} + ${mem} * 3600")"
                ;;
            'd' | 'day' | 'days')
                sec="$(bc -s <<< "scale=6; ${sec} + ${mem} * 86400")"
                ;;
            'w' | 'week' | 'weeks')
                sec="$(bc -s <<< "scale=6; ${sec} + ${mem} * 604800")"
                ;;
            'M' | 'month' | months)
                sec="$(bc -s <<< "scale=6; ${sec} + ${mem} * 259200")"
                ;;
            'y' | 'year' | 'years')
                mem="$(bc -s <<< "scale=0; ${mem} / 1")"
                warn "Warning: Only the integer part of 'year' will be preserved." 1>&2
                sec="$(bc -s <<< "scale=6; ${sec} + ${mem} * 31536000")"
                ;;
            *)
                warn "Warning: Can't parse time: ${item##*'-'} ${item%%'-'*}"
        esac
    done

    printf '%s' "${sec}"
}

# Flush comments
while read -r line; do
    if [ "${line:0:1}" = '#' ] || [ "${line:0:1}" = ';' ]; then
        # Just discard comments
        :
    elif [ "${line:0:1}" != '[' ]; then
        KVS+=("${line}")
    fi
done < "${UNITFILE}"

# Actual converting, where fun begins
declare -i IS_PIDFILE=0
declare -i HAS_TYPE=0
: >| "${UNITFILE}.dinit"
write() { printf '%s\n' "$1" >> "${UNITFILE}.dinit"; }
for kv in "${KVS[@]}"; do
    KEY="$(sed -E 's/^[[:blank:]]*//; s/[[:blank:]]*$//' <<< "${kv%%'='*}")"
    VALUE="$(sed -E 's/^[[:blank:]]*//; s/[[:blank:]]*$//' <<< "${kv##*'='}")"
    declare -i EXISTENCE=0
    for item in "${SYSTEMD_REF_MAP[@]}"; do
        if [ "${item}" = "${KEY}" ]; then
            EXISTENCE=1
        fi
    done
    if [ "${EXISTENCE}" -ne 1 ] && [ -n "${KEY}" ]; then
        warn "Unknown or unsupported key: ${KEY}"
    else
        case "${KEY}" in
            'Documention')
                continue
                ;;
            'Description')
                write "# Description: ${VALUE}"
                ;;
            'Type')
                case "${VALUE}" in
                    'simple' | 'exec')
                        write 'type = process'
                        ;;
                    'forking')
                        write 'type = bgprocess'
                        IS_PIDFILE=1
                        ;;
                    'oneshot')
                        write 'type = scripted'
                        ;;
                    'notify')
                        write 'type = process'
                        warn 'It seems this service uses systemd activation protocol. However, it is NOT SUPPORTED by dinit. Please change it to use a proper notification protocol.'
                        ;;
                    'dbus')
                        warn 'DBus is NOT SUPPORTED by dinit.'
                        exit 1
                esac
                declare -i HAS_TYPE=1
                ;;
            'ExecStart')
                write "command = ${VALUE}"
                ;;
            'ExecStop')
                write "stop-command = ${VALUE}"
                ;;
            'Wants' | 'UpHolds')
                for dep in ${VALUE}; do
                    write "waits-for = ${dep}"
                done
                ;;
            'Requires' | 'Requisite' | 'BindsTo' | 'PartOf')
                for dep in ${VALUE}; do
                    write "depends-on = ${dep}"
                done
                ;;
            'WantedBy' | 'RequiredBy' | 'UpheldBy')
                for dep in ${VALUE}; do
                    write "depends-ms = ${dep}"
                done
                ;;
            'Before')
                for dep in ${VALUE}; do
                    write "before = ${dep}"
                done
                ;;
            'After')
                for dep in ${VALUE}; do
                    write "after = ${dep}"
                done
                warn 'Remember: AFTER in dinit HAS DIFFERENT FUNCTIONALITY over systemd.'
                ;;
            'Alias')
                warn 'Ignoring aliases...'
                ;;
            'OnSuccess')
                for chain in ${VALUE}; do
                    write "chain-to ${chain}"
                done
                ;;
            'StartLimitBurst')
                write "restart-limit-count = ${VALUE}"
                ;;
            'StartLimitIntervalSec')
                write "restart-limit-interval = $(parse_time "${VALUE}")"
                ;;
            'PIDFile')
                write "pid-file = ${VALUE}"
                IS_PIDFILE=2
                ;;
            'EnvironmentFile')
                write "env-file = ${VALUE}"
                ;;
            'Restart')
                if [ "${VALUE}" = 'no' ]; then
                    write 'restart = no'
                else
                    write 'restart = no'
                fi
                ;;
            'TimeoutStartSec' | 'TimeoutStopSec' | 'TimeoutSec')
                if [ "${VALUE}" = 'infinity' ]; then
                    TIME=0
                else
                    TIME="$(parse_time "${VALUE}")"
                fi
                if [ "${KEY}" = 'TimeoutSec' ]; then
                    write "start-timeout = ${TIME}\nstop-timeout = ${TIME}"
                elif [ "${KEY}" = 'TimeoutStartSec' ]; then
                    write "start-timeout = ${TIME}"
                else
                    write "stop-timeout = ${TIME}"
                fi
                ;;
            'User')
                write "run-as = ${VALUE}:${VALUE}"
                ;;
            'Group')
                :
                ;;
            'WorkingDirectory')
                write "working-dir = ${VALUE}"
                ;;
            'LimitCORE')
                write "rlimit-core = ${VALUE}"
                ;;
            'LimitNOFILE')
                write "rlimit-nofile = ${VALUE}"
                ;;
            'LimitDATA')
                write "rlimit-data = ${VALUE}"
                ;;
            'UtmpIdentifier')
                write "inittab-line = ${VALUE}"
                ;;
            'KillSignal')
                write "term-signal = ${VALUE#'SIG'}"
                ;;
            *)
                [ -n "${KEY}" ] && warn "Not implemented: ${KEY}"
                ;;
        esac
    fi
done

if [ "${HAS_TYPE}" -ne 1 ]; then
    write 'type = process'
fi

if [ "${IS_PIDFILE}" -eq 1 ]; then
    warn 'It seems this service DOES NOT have a PID file. This is NOT EXPECTED in dinit.'
fi

warn 'Converting service unit to dinit service is completed.'
warn 'It is HIGHLY recommended to modify this generated file to fit your needs.'
