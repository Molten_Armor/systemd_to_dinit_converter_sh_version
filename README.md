unit_to_srv: Covnert systemd unit into dinit service
----------------------------------------------------

This is a bash script (Requires bash 4.0 or later) for easily converting systemd
unit services into dinit services.

## Minimal Usage
```
Usage: unit_to_srv.sh <Unit file>
```
